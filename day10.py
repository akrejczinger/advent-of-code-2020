#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The tasks here are difficult to explain in short, see day10_tasks.txt
"""
from collections import Counter
from functools import reduce
from operator import mul


def diffs(lst):
    diffs = []
    for i in range(len(lst) - 1):
        diff = abs(lst[i] - lst[i+1])
        diffs.append(diff)
    return diffs


def task1(lines):
    chosen = sorted(lines)
    # The list always starts with 0 (outlet) and ends with the device which is 3 higher than last
    chosen.insert(0, 0)
    chosen.append(chosen[-1] + 3)
    print(chosen)
    d = diffs(chosen)
    print(d)
    counts = Counter(d)
    print(counts)
    print(counts[1] * counts[3])


def count_splits_of_3(diffs):
    """
    Count how many 1s there are, separated by 3s.
    Example: [1, 1, 3, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 1, 3]
    This produces: [2, 1, 4, 0, 4]
    """
    def split_at(lst, val):
        res = []
        acc = []
        for num in lst:
            if num == val:
                res.append(acc)
                acc = []
            else:
                acc.append(num)
        return res
    splits = split_at(diffs, 3)
    return [len(sublist) for sublist in splits]


def count_combo_1s(num):
    """
    Count how many different ways `num` number of adapters with 1-jolt diffs
    in a row can be arranged, if not all of them have to be used.
    Example: 4 adapters with e.g. jolts 4,5,6,7 can be arranged like:
    - diffs: 1, 1, 1, 1     jolts 4, 5, 6, 7
    - diffs: 1, 1, 2        jolts 5, 6, 7
    - diffs: 1, 2, 1        jolts 4, 6, 7
    - diffs: 2, 1, 1        jolts 4, 5, 7
    - diffs: 2, 2           jolts 5, 7
    - diffs: 1, 3           jolts 4, 7
    - diffs: 3, 1           jolts 6, 7
    Total: 7 different ways. The actual jolt numbers are irrelevant, only shown as an example.
    """
    # Let's try brute-force: add neighbors together in each loop
    # Initialized combos with the first element: all 1s
    combos = {0: {tuple([1] * num)}}
    # The 'j' loop counter shows how many times the neighbors were combined
    for j in range(1, num+1):
        # check if we ran out of valid digits to combine together
        if j-1 not in combos:
            break
        # Copy the combos from previous iteration and combine them further
        for combo in combos[j-1].copy():
            lst = list(combo)
            for i in range(len(lst) - 1):
                summed = lst[i] + lst[i+1]
                # Create a list with the given two elements summed
                combined = tuple(lst[:i] + [summed] + lst[i+2:])
                # If there is no element which is too high, add it to the valid combos
                if all([x < 4 for x in combined]):
                    if j not in combos:
                        combos[j] = set()
                    combos[j].add(combined)

    # Count the number of found combos and return
    num_of_combos = 0
    for combo_set in combos.values():
        num_of_combos += len(combo_set)
    return num_of_combos


def task2(lines):
    chosen = sorted(lines)
    # The list always starts with 0 (outlet) and ends with the device which is 3 higher than last
    chosen.insert(0, 0)
    chosen.append(chosen[-1] + 3)
    d = diffs(chosen)
    # NOTICE: the output only contains diffs of 1 or 3, no 2's. This can maybe help simplify the
    # solution.
    # Idea: look at the diffs (excerpt): [1, 1, 3, 1, 3, 1, 1, 1, 1, 3, 3, 1, 1, 1, 1, 3,...]
    # The number 3 forms "walls" where there is only one choice: the next adapter with diff of 3.
    # In the other places, the 1s can be arranged in many ways: grouped up to 3 at a time, which
    # would mean skipping 2 adapters of 1-jolt diff, and going with an adapter 3 jolt higher.
    # The idea is to split the list at 3s, count how many 1s are there inbetween, count how many
    # ways each of these little lists can be arranged, then multiply them all together.
    splits = (count_splits_of_3(d))
    counted_combos = [count_combo_1s(x) for x in splits]
    print('Combo counts:', counted_combos)
    product = reduce(mul, counted_combos, 1)
    print('Multiplied:', product)


if __name__ == '__main__':
    with open('day10_input.txt', 'r') as f:
        lines = f.readlines()
    lines = [int(line) for line in lines]

    task2(lines)
