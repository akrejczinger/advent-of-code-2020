#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from functools import reduce
from operator import mul
"""
Task 1:

There is a text file given. The text file shows a map of empty squares (.) and trees (#).
The map is repeating (we should consider this map to tile infinitely to the right).
Starting from the top-left corner, always going 3 squares to the right and 1 down, how many trees
will be on the way until we reach the bottom?

Task 2:

Calculate the number of trees with various different slopes and multiply the numbers together.
"""


def check_slope(slope):
    # this was task1, with righ 3 and down 1 as the slope
    right, down = slope
    with open('day3_input.txt', 'r') as f:
        x = 0
        y = 0
        line_width = 0
        trees = 0
        for line in f:
            if y % down != 0:
                y += 1
                continue
            if line_width == 0:
                line_width = len(line.strip())
            if line[x] == '#':
                trees += 1
            elif line[x] == '.':
                pass
            else:
                raise ValueError(f'Unknown map tile: {line[x]}')
            x = (x + right) % line_width
            y += 1

    return trees


def task_2():
    slopes = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ]
    trees = list(map(check_slope, slopes))
    print(f'Trees for slope of 1, 3, 5, 7, 1/2: {trees}')
    print(f'Multiplied: {reduce(mul, trees, 1)}')


if __name__ == '__main__':
    task_2()
