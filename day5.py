#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task 1: Binary search of plane seating.
First 8 characters: rows go from 0 to 127. F means lower half, B means upper half.
With binary search , find the row of the seats.
Last 3 characters: same idea, with the 8 columns.
From the row and column, seat id is row * 8 + column. Find the highest seat ID in the list.

Task 2: The flight is full. The seat with no boarding pass will be the one we are looking for.
Some seats near the front and back don't exist so they don't appear in the list. We check for this
by using the fact that ID+1 and ID-1 from our seat exists (we are not at the very front or back).
"""


def binsearch(binary, lower_char, upper_char):
    digits = len(binary)
    lower = 0
    upper = 2 ** digits - 1

    for char in binary:
        # special edge case: last 2 chars
        if upper - lower == 1:
            if char == lower_char:
                return lower
            elif char == upper_char:
                return upper

        split = round((upper - lower) / 2)
        if char == lower_char:
            upper -= split
        elif char == upper_char:
            lower += split


def get_row(binary):
    return binsearch(binary, 'F', 'B')


def get_col(binary):
    return binsearch(binary, 'L', 'R')


def get_seat_info(binary):
    row_binary = binary[:7]
    col_binary = binary[7:]

    row = get_row(row_binary)
    col = get_col(col_binary)

    seat_id = row * 8 + col
    return {
        'row': row,
        'col': col,
        'id': seat_id,
    }


def task1():
    info_with_max = {'id': 0}
    with open('day5_input.txt', 'r') as f:
        for line in f:
            info = get_seat_info(line.strip())
            print(info)
            if info['id'] > info_with_max['id']:
                info_with_max = info
    print('MAX: ----------------')
    print(info_with_max)


def task2():
    data = dict()
    with open('day5_input.txt', 'r') as f:
        for line in f:
            info = get_seat_info(line.strip())
            seat_id = info['id']
            data[seat_id] = info

    # find seats with missing boarding pass
    missing_ids = []
    for i in range(128 * 8):
        if i not in data:
            missing_ids.append(i)

    # find which missing id has id+1 and id-1 existing
    for i in missing_ids:
        if i+1 not in data or i-1 not in data:
            continue
        print(i)
        break


if __name__ == '__main__':
    task2()
