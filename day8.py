#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task1:
Given the assembly instructions in the input file, check the value of the accumulator immediately
before the program enters an infinite loop (executes a line that it already executed once).

Task2:
The program exits normally if attempting to run an instruction exactly 1 line below the last line.
Make the program run by swapping exactly one NOP to JMP or JMP to NOP. Values and ACC instructions
are unchanged. What is the final accumulator value when the modified program exits?
"""


def parse_line(line):
    instr, val_str = line.strip().split()
    val = int(val_str)
    return instr, val


def execute(line, acc, prog_ptr):
    instr, val = parse_line(line)
    if instr == 'nop':
        # do nothing, jump to next line
        prog_ptr += 1
    elif instr == 'acc':
        acc += val
        prog_ptr += 1
    elif instr == 'jmp':
        prog_ptr += val
    else:
        raise ValueError(f'Unknown instruction: {instr} {val:+d}')

    return acc, prog_ptr


def does_halt_normally(prog_lines):
    acc = 0
    prog_ptr = 0
    lines_seen = [False] * len(prog_lines)
    for max_instr in range(1_000_000):  # limit execution to max 1 million instructions
        if prog_ptr >= len(prog_lines):
            if len(prog_lines) == prog_ptr:
                print(f'Executed successfully, acc value: {acc}')
                return True
            else:
                print(f'Jumping beyond program code. Prog len: {len(prog_lines)}. Ptr: {prog_ptr}.  Acc: {acc}')
                return False
        if lines_seen[prog_ptr] is True:
            # reached infinite loop, print acc and exit
            print(f'Loop detected at ptr {prog_ptr}. Accumulator value: {acc}')
            return False
        else:
            lines_seen[prog_ptr] = True
            acc, prog_ptr = execute(prog_lines[prog_ptr], acc, prog_ptr)
    else:
        print('Max instruction count reached')


def swap(line):
    if line.startswith('nop'):
        return line.replace('nop', 'jmp')
    else:
        return line.replace('jmp', 'nop')


def task2(prog_lines):
    # collect all nop and jmp instruction indices which can be swapped
    modifiable_ptrs = []
    for ptr, line in enumerate(prog_lines):
        if line.startswith('nop') or line.startswith('jmp'):
            modifiable_ptrs.append(ptr)

    # loop through all these instructions, and run them through the task1 loop detector in turn.
    for m in modifiable_ptrs:
        mod_lines = prog_lines[:]  # make a copy
        mod_lines[m] = swap(mod_lines[m])
        halted = does_halt_normally(mod_lines)
        if halted:
            # When halted successfully, stop iterating.
            break


if __name__ == '__main__':
    with open('day8_input.txt', 'r') as f:
        prog_lines = f.readlines()

    # number of jmp instr.: 225
    # number of nop instr.: 86

    # task1:
    # res = does_halt_normally(prog_lines)

    task2(prog_lines)
