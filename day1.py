#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task part 1: find two numbers in the given list that sum up to 2020.
Multiply them together and print the result.

Task part 2: find THREE numbers that sum up to 2020.
Multiply them together and print the result.
"""


def part_one():
    numbers = set()
    with open('day1_input.txt', 'r') as f:
        for line in f:
            num = int(line)
            diff = 2020 - num
            if diff in numbers:
                # found the multipliers
                print(diff * num)
                break
            else:
                numbers.add(num)


def part_two():
    numbers = list()
    candidates = list()
    with open('day1_input.txt', 'r') as f:
        for line in f:
            num = int(line)
            numbers.append(num)
            print(f'Next number: {num}')

            # sum with all candidate pairs. If found 2020 sum: done.
            for first, second in candidates:
                if first + second + num == 2020:
                    print(f'Found sum: {first}, {second}, {num}')
                    print(first * second * num)
                    exit(0)

            # create more candidate pairs.
            for first in numbers:
                if first + num < 2020:
                    print(f'Adding candidate: {first}, {num}')
                    candidates.append((first, num))


if __name__ == '__main__':
    part_two()
