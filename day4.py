#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task1:

validate passports given in the input file.
The data is given as key-value pairs separated by ':'. Each key-value pair is separated by space or
a single newline. Passports are separated by a blank line (2 newlines).

Mandatory fields:
    - byr (Birth Year)
    - iyr (Issue Year)
    - eyr (Expiration Year)
    - hgt (Height)
    - hcl (Hair Color)
    - ecl (Eye Color)
    - pid (Passport ID)
Optional fields:
    - cid (Country ID)
"""
import re
HCL_RE = re.compile('^#[0-9a-f]{6}$')
PID_RE = re.compile('^[0-9]{9}$')
MANDATORY_FIELDS = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}


def parse_passports(filename):
    """
    Passport ID (pid) selected as key for the dictionary. Entries with no pid will be dropped.
    """
    def _process_fields(_line):
        out = dict()
        pairs = _line.split(' ')
        for pair in pairs:
            key, val = pair.split(':')
            out[key.strip()] = val.strip()
        return out

    data = dict()
    with open(filename, 'r') as f:
        current_entry = dict()
        for line in f:
            if line.strip() != '':
                current_entry.update(_process_fields(line))
            else:
                # insert into dict if pid found
                if 'pid' in current_entry:
                    pid = current_entry['pid']
                    data[pid] = current_entry
                else:
                    print(f'WARNING: no pid, dropped entry: {current_entry}')
                # clear current entry, ready for processing the next one
                current_entry = dict()
    # insert last entry collected
    pid = current_entry.get('pid')
    if pid:
        data[pid] = current_entry
    return data


def mandatory_fields_exist(entry):
    # count all missing mandatory fields
    return len(MANDATORY_FIELDS - set(entry.keys())) == 0


def is_valid_field(key, val):
    """
    byr: 1920 <= x <= 2002
    iyr: 2010 <= x <= 2020
    eyr: 2020 <= x <= 2030
    hgt: ends with 'cm' or 'in'.
        cm: 150 <= x <= 193
        in: 59 <= x <= 76
    hcl: starts with #, then six hexadecimal lowercase chars
    ecl: one of ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    pid: 9-digit num, including leading zeroes
    cid: ignored, may be missing
    """
    def _int(x):
        try:
            return int(x)
        except ValueError:
            return None

    # TODO these validators could be a dict of lambdas or something?
    if key == 'byr':
        val = _int(val)
        if val is None:
            return False
        return 1920 <= val <= 2002
    elif key == 'iyr':
        val = _int(val)
        if val is None:
            return False
        return 2010 <= val <= 2020
    elif key == 'eyr':
        val = _int(val)
        if val is None:
            return False
        return 2020 <= val <= 2030
    elif key == 'hgt':
        if val.endswith('cm'):
            val = _int(val.strip('cm'))
            if val is None:
                return False
            return 150 <= val <= 193
        elif val.endswith('in'):
            val = _int(val.strip('in'))
            if val is None:
                return False
            return 59 <= val <= 76
        else:
            return False
    elif key == 'ecl':
        return val in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}
    elif key == 'hcl':
        m = HCL_RE.match(val)
        return True if m else False
    elif key == 'pid':
        m = PID_RE.match(val)
        return True if m else False
    elif key == 'cid':
        return True  # cid may be anything, may be missing too
    else:
        return True  # ignore other keys


def task1():
    data = parse_passports('day4_input.txt')

    valid_pass = 0
    for pid, entry in data.items():
        if mandatory_fields_exist(entry):
            valid_pass += 1
    print(valid_pass)


def task2():
    data = parse_passports('day4_input.txt')

    valid = 0
    for entry in data.values():
        print('--------------------------------')
        print(f'Validating entry {entry}')
        if not mandatory_fields_exist(entry):
            print(f'Some fields missing for entry with pid {entry["pid"]}')
            continue
        try:
            for field in MANDATORY_FIELDS:
                if is_valid_field(field, entry[field]):
                    print(f'Field {field} value valid: {entry[field]}')
                else:
                    print(f'Field {field} value WRONG: {entry[field]}')
                    raise StopIteration
            valid += 1
        except StopIteration:
            pass  # break the loop
        print(f'Valid so far: {valid}')

    print('Final:')
    print(valid)


if __name__ == '__main__':
    task2()
