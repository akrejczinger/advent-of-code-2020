#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task1: There are groups of people. They answer questions called a-z. Each person's YES answers are
on a separate line, each group is separated by a blank line.
Count how many questions at least one person within each group answered as YES. Sum these together.

Task2: same as above, but count how many questions EVERYONE in a group answered as YES.
"""


def task1():
    data = []
    cur_group_answers = set()
    with open('day6_input.txt', 'r') as f:
        for line in f:
            if line.strip() == '':
                data.append(cur_group_answers)
                cur_group_answers = set()
                continue
            cur_group_answers |= set(line.strip())
    # last data appended
    data.append(cur_group_answers)
    counts = [len(s) for s in data]
    print(counts)
    print(sum(counts))


def task2():
    data = []
    cur_group_answers = None
    with open('day6_input.txt', 'r') as f:
        for line in f:
            if line.strip() == '':
                if cur_group_answers is not None:
                    print(f'NOT NONE, appending: {cur_group_answers}')
                    data.append(cur_group_answers)
                print('---------NEW GROUP------------------')
                cur_group_answers = None
                continue
            if cur_group_answers is None:
                cur_group_answers = set(line.strip())
                print(f'Empty set, adding first entry: {cur_group_answers}')
            else:
                cur_group_answers &= set(line.strip())
                print(f'Nonempty set, intersect {set(line.strip())}: {cur_group_answers}')
    # last data appended
    data.append(cur_group_answers)
    counts = [len(s) for s in data]
    print(counts)
    print(sum(counts))


if __name__ == '__main__':
    task2()
