#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Task1: there is a list of rules given, what kind of bag can hold what kind of other bags.
The task: figure out how many kinds of bags can eventually contain a shiny gold bag.

Example rules:
light chartreuse bags contain 1 mirrored yellow bag, 2 vibrant violet bags.
dotted silver bags contain 2 dotted orange bags, 3 bright fuchsia bags, 5 bright tomato bags, 3 faded turquoise bags.
muted brown bags contain no other bags.
"""
import re


def parse_bag_contents():
    bag_content_data = dict()  # key = bag name, values = count and name of CONTENT bags
    containing_bags_data = dict()  # key = bag name, values = name of CONTAINING bags
    with open('day7_input.txt', 'r') as f:
        for line in f:
            bag_type_match = re.match(r'(\w+ \w+) bags contain', line)
            content_match = re.findall(r'(\d+) (\w+ \w+) bags?', line)

            containing_bag_type = bag_type_match.group(1)
            bag_content_data[containing_bag_type] = []
            for count, bag_type in content_match:
                bag_content_data[containing_bag_type].append((bag_type, count))
                if bag_type not in containing_bags_data:
                    containing_bags_data[bag_type] = set()
                containing_bags_data[bag_type].add(containing_bag_type)
    return bag_content_data, containing_bags_data


def containing_search(containing_bags, bag_type):
    """
    Collect all containing bags in a set recursively.
    """
    next_bags = containing_bags.get(bag_type, set())
    more_bags = set()
    for next_bag in next_bags:
        more_bags |= (containing_search(containing_bags, next_bag))
    return set(next_bags) | more_bags


def contents_search(content_data, bag_type, count=1):
    acc = {
        'name': bag_type,
        'count': count,
        'contents': [],
    }
    next_level = content_data.get(bag_type, [])
    for bag_name, count in next_level:
        count = int(count)
        acc['contents'].append(contents_search(content_data, bag_name, count=count))
    return acc


def count_bags(content_tree):
    this_count = content_tree['count']
    sub_count = 0
    for data in content_tree['contents']:
        sub_count += count_bags(data)
        print(data['name'], sub_count)
    # NOTE: count the bags at THIS level as well as the lower levels multiplied!
    return this_count * (1 + sub_count)


def task1():
    contents, containing = parse_bag_contents()
    res = containing_search(containing, 'shiny gold')
    print(res)
    print(len(res))


def task2():
    contents, containing = parse_bag_contents()
    from pprint import pprint
    content_tree = contents_search(contents, 'shiny gold')
    pprint(content_tree)
    print('---------------------------------')
    # NOTE: 'off by one error': the shiny gold bag itself is counted,
    # but only the CONTENTS are needed. So we subtract 1.
    print(count_bags(content_tree) - 1)
    # 35488: too high


if __name__ == '__main__':
    task2()
