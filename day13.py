#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def task1(schedule):
    now = int(schedule[0])
    sched = sorted([int(x) for x in schedule[1].strip().split(',') if x != 'x'])
    minutes_to_depart = dict()
    min_id = sched[0]
    min_time = None
    for bus_id in sched:
        last_depart = now % bus_id
        time_to_depart = bus_id - last_depart
        minutes_to_depart[bus_id] = time_to_depart
        if min_time is None or time_to_depart < min_time:
            min_id = bus_id
            min_time = time_to_depart
    print(minutes_to_depart)
    m = min_id * min_time
    print(f'Minimum: {min_time} minutes with bus id {min_id}, multiplied: {m}')


def brute_mod(start, mult, mod, target):
    """
    Brute-force calculate the smallest possible integer "n" that fulfills this equation:
    (start + n * mult) % mod == target
    """
    cur = start
    # check if the equation is already satisfied
    if cur % mod == target:
        return 0

    # max possible iter. count is the modulo itself (due to how modular arithmetic works, n*a is
    # guaranteed to be divisible by n)
    for i in range(mod):
        cur += mult
        if cur % mod == target:
            return i + 1


def task2(schedule):
    # map of bus id -> time offset
    sched = dict()
    for i, x in enumerate(schedule[1].strip().split(',')):
        if x != 'x':
            sched[int(x)] = i
    print(sched)

    """
    This mess does the following:
    Given a start value, a multiplier to keep adding to the sum, a modulo, and a target remainder
    value, it finds how many times the multiplier has to be added to the sum to reach the current
    modulo criteria. Then adds the multiplier times 'n' to the previous sum.
    In the next iteration this number will be where we start from, and the new multiplier is all the
    previous multipliers MULTIPLIED (this way the previous modulos don't change).
    It looks messy but works.
    """
    multipliers = list(sorted(sched.keys()))
    prev_mult = multipliers.pop()
    prev_start = prev_mult - sched[prev_mult]
    print('starting mult', prev_mult)
    print('starting start', prev_start)
    for mult in reversed(multipliers):
        print('mult', mult)
        offset = sched[mult]
        print('offset', offset)
        start = prev_start + offset
        print('start', start)
        n = brute_mod(start, prev_mult, mult, 0)
        print('n', n)
        result = prev_start + prev_mult * n
        print('-> result', result)

        # next iteration starts from the current result constant
        prev_start = result
        # we always multiply by all the previous multipliers as well, to not change the remainders
        prev_mult *= mult


if __name__ == '__main__':
    with open('day13_input.txt', 'r') as f:
        schedule = f.readlines()
    task2(schedule)
