#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def task1(numbers):
    """
    The first 25 numbers are the preamble. Find the number from the rest of the numbers which is not
    a sum of any 2 of the 25 numbers that comes before in the list.
    """
    # for each number: add it to all 25 prev. numbers, store these in a set.
    # for each number after 25th: check if this number appears in any of the 25 prev. saved sets
    # if not, print that number
    sums = []
    for i, n in enumerate(numbers):
        if i == 0:
            # first number has no sums
            sums.append(set())
            continue

        # build the current sums set and check if the number is valid, in the same loop
        cur_sums = set()
        is_valid = False
        lower_index = max(i-26, 0)  # make sure we don't underflow the array
        for j in range(i-1, lower_index - 1, -1):
            cur_sums.add(numbers[i] + numbers[j])
            if n in sums[j]:
                is_valid = True
        if not is_valid and i >= 25:
            # found the non-conforming number
            print(n)
            return
        sums.append(cur_sums)


def task2(numbers):
    """
    Find a contiguous set of at least two numbers which sum to the found invalid number.
    """
    # idea: FIFO queue-like sequence.
    # Add numbers at the end of the list until we reach or overshoot the given number.
    # Then remove numbers from the beginning until we go below again, and iterate until matches.
    invalid_num = 400480901
    seq = []
    mysum = 0  # keep track of the mysum manually, micro-optimisation
    for i, n in enumerate(numbers):
        seq.append(n)
        mysum += n
        while mysum > invalid_num:
            first = seq.pop(0)
            mysum -= first
        if mysum == invalid_num:
            print(f'Found sequence {seq} at index {i}')
            smallest = min(seq)
            largest = max(seq)
            print(f'Smallest: {smallest}, largest: {largest}, sum: {smallest+largest}')
            return


if __name__ == '__main__':
    numbers = []
    with open('day9_input.txt', 'r') as f:
        for line in f:
            numbers.append(int(line))

    task2(numbers)
