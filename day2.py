#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re

RE_PATTERN = re.compile(r'(\d+)-(\d+) (\w): (\w+)')


def parse_line(line):
    r"""
    Format of each line:
    2-6 c: fcpwjqhcgtffzlbj
    ^ ^ ^  ^-password
    | | \-password policy applies to this character
    | \-maximum allowed count of character
    \-minimum allowed count of character

    :return: regex match groups of the form (min, max, char, password)
    """
    m = RE_PATTERN.match(line)
    assert m is not None
    return m.groups()


def task_1():
    """
    Count the number of passwords that match the respective validation pattern,
    where each pattern describes the minimum and maximum number of occurrences of each character.
    """
    valid_passwords = 0
    with open('day2_input.txt', 'r') as f:
        for line in f:
            char_min, char_max, char, password = parse_line(line)
            char_min = int(char_min)
            char_max = int(char_max)
            if char_min <= password.count(char) <= char_max:
                valid_passwords += 1
    print(valid_passwords)


def task_2():
    """
    Count the number of passwords that match the respective validation pattern,
    where each pattern describes the first and second index, the character and the password.
    The character may only occur once in the first and second index. The characters in the other
    indices of the password are irrelevant (the character may appear any number of times there).
    The indices start at 1, not 0!
    """
    valid_passwords = 0
    with open('day2_input.txt', 'r') as f:
        for line in f:
            i1, i2, char, password = parse_line(line)
            # convert indices to zero-based
            i1 = int(i1) - 1
            i2 = int(i2) - 1
            first_match = password[i1] == char
            second_match = password[i2] == char
            if first_match ^ second_match:  # xor
                valid_passwords += 1
    print(valid_passwords)


if __name__ == '__main__':
    task_2()
