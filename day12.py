#!/usr/bin/env python3
# -*- coding: utf-8 -*-

FACINGS = ['E', 'N', 'W', 'S']


class Waypoint:
    """
    Basically a direction vector where the ship moves for each 'F' instruction in task2.
    """
    def __init__(self, x=10, y=1):
        self.x = x
        self.y = y

    def rotate(self, l_or_r, value):
        for _ in range(value, 0, -90):  # one rotation every 90 degrees
            tmp_x, tmp_y = self.x, self.y
            if l_or_r == 'L':
                self.x = -1 * tmp_y
                self.y = tmp_x
            else:
                self.x = tmp_y
                self.y = -1 * tmp_x

    def move(self, facing, value):
        if facing == 'N':
            self.y += value
        elif facing == 'S':
            self.y -= value
        elif facing == 'E':
            self.x += value
        elif facing == 'W':
            self.x -= value
        elif facing == 'L':
            self.rotate('L', value)
        elif facing == 'R':
            self.rotate('R', value)
        else:
            raise ValueError(f'Unknown action for waypoint: {facing}{value}')


class Ship:
    """
    Basic ship with no waypoint.
    """
    def __init__(self, x=0, y=0, facing='E'):
        self.x = x
        self.y = y
        self.facing = 'E'

    def rotate(self, l_or_r, value):
        i = FACINGS.index(self.facing)
        for _ in range(value, 0, -90):  # one rotation every 90 degrees
            if l_or_r == 'L':
                i += 1
            else:
                i -= 1
            if i >= len(FACINGS):
                i = len(FACINGS) - i
            if i < 0:
                i = len(FACINGS) + i
        self.facing = FACINGS[i]

    def move(self, facing, value):
        if facing == 'N':
            self.y += value
        elif facing == 'S':
            self.y -= value
        elif facing == 'E':
            self.x += value
        elif facing == 'W':
            self.x -= value
        elif facing == 'L':
            self.rotate('L', value)
        elif facing == 'R':
            self.rotate('R', value)
        elif facing == 'F':
            self.move(self.facing, value)
        else:
            raise ValueError(f'Unknown action: {facing}{value}')


class WaypointShip(Ship):
    """
    Ship that uses a waypoint for navigation.
    """
    def __init__(self, waypoint, x=0, y=0):
        self.x = x
        self.y = y
        self.waypoint = waypoint

    def move(self, facing, value):
        if facing == 'F':
            # move based on waypoint
            self.x += self.waypoint.x * value
            self.y += self.waypoint.y * value
        else:
            # move waypoint itself
            self.waypoint.move(facing, value)


def task1(actions):
    ship = Ship(x=0, y=0, facing='E')
    for action in actions:
        action = action.strip()
        facing, value = action[0], action[1:]
        ship.move(facing, int(value))
    print(f'Ship is at {ship.x}:{ship.y}')
    print(f'Manhattan distance: {abs(ship.x) + abs(ship.y)}')


def task2(actions):
    waypoint = Waypoint(x=10, y=1)
    ship = WaypointShip(waypoint)
    for action in actions:
        action = action.strip()
        facing, value = action[0], action[1:]
        ship.move(facing, int(value))
    print(f'Ship is at {ship.x}:{ship.y}')
    print(f'Manhattan distance: {abs(ship.x) + abs(ship.y)}')


if __name__ == '__main__':
    with open('day12_input.txt', 'r') as f:
        actions = f.readlines()
    task2(actions)
