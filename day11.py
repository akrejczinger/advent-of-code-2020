#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def count_neighbors(seats, x, y):
    """
    :param seats: list of strings, the arrangement of seats
    :param x: zero-indexed number of column of seat
    :param y: zero-indexed number of row of seat

    Indices to check:
    x-1, y-1 | x,   y-1 | x+1, y-1
    x-1, y   | (x,   y) | x+1, y
    x-1, y+1 | x,   y+1 | x+1, y+1

    The middle x,y is ignored. Any index outside the seating area is ignored.
    """
    max_x = len(seats[0])
    max_y = len(seats)

    def check_bounds(_index):
        x, y = _index
        return 0 <= x < max_x and 0 <= y < max_y

    indices = [
        (x-1, y-1), (x, y-1), (x+1, y-1),
        (x-1, y), (x+1, y),
        (x-1, y+1), (x, y+1), (x+1, y+1),
    ]
    ind = list(filter(check_bounds, indices))
    neighbors = 0
    for x, y in ind:
        if seats[y][x] == '#':
            neighbors += 1
    return neighbors


def update_seats_task1(seats):
    updated_seats = []
    for y, row in enumerate(seats):
        updated_row = []
        if isinstance(row, str):
            # strip newline and whitespace
            row = row.strip()
        for x, seat in enumerate(row):
            if seat == 'L':
                # If seat is empty: check if neighbors zero, fill if so
                n = count_neighbors(seats, x, y)
                if n == 0:
                    # print(f'{x}:{y} [{seat}] -> No neighbors, filling seat')
                    updated_row.append('#')
                else:
                    # no change
                    updated_row.append(seat)
            elif seat == '#':
                # If seat is occupied: check if neighbors >= 4, empty if so
                n = count_neighbors(seats, x, y)
                if n >= 4:
                    # print(f'{x}:{y} [{seat}] -> {n} neighbors, emptying seat')
                    updated_row.append('L')
                else:
                    # no change
                    updated_row.append(seat)
            else:
                # No change
                # print(f'{x}:{y} [{seat}] -> No change')
                updated_row.append(seat)
        updated_seats.append(updated_row)
    return updated_seats


def task1(lines):
    # '#' means occupied, 'L' means empty seat, '.' means floor.
    state = lines.copy()
    next_state = update_seats_task1(state)
    i = 0
    while next_state != state:
        if i == 60000:
            print('Iter limit reached')
            break
        state = next_state
        next_state = update_seats_task1(state)
        print('\r' + str(i), end='')
        i += 1
    print('\nFound stable state.')
    n = 0
    for row in state:
        for seat in row:
            if seat == '#':
                n += 1
    for row in state:
        print(''.join(row))
    print(f'Number of filled seats: {n}')


def count_visible_seats(seats, x, y):
    """
    Check each direction until seat found (either empty or filled, floor doesnt count)
    or reaching boundary.
    """
    max_x = len(seats[0])
    max_y = len(seats)

    dir_vectors = [
        (-1, -1),
        (0, -1),
        (1, -1),
        (-1, 0),
        (1, 0),
        (-1, 1),
        (0, 1),
        (1, 1),
    ]
    occupied_visible = 0
    for dx, dy in dir_vectors:
        x1, y1 = x, y
        while True:
            x1 += dx
            y1 += dy
            if not (0 <= x1 < max_x and 0 <= y1 < max_y):
                # reached boundary
                # print(f'Reached boundary at {x1}:{y1}')
                break
            seat = seats[y1][x1]
            if seat == '#':
                occupied_visible += 1
                # print(f'Visible # at {x1}:{y1}, incremented to {occupied_visible}')
                break
            elif seat == 'L':
                # found unoccupied seat -> don't count, but stop iterating
                # print(f'Visible L at {x1}:{y1}, no increment')
                break
            elif seat == '.':
                # no seat found yet
                # print(f'No seat (.) at {x1}:{y1}')
                continue
    return occupied_visible


def update_seats_task2(seats):
    updated_seats = []
    for y, row in enumerate(seats):
        updated_row = []
        if isinstance(row, str):
            # strip newline and whitespace
            row = row.strip()
        for x, seat in enumerate(row):
            if seat == 'L':
                # If seat is empty: check if neighbors zero, fill if so
                n = count_visible_seats(seats, x, y)
                if n == 0:
                    # print(f'{x}:{y} [{seat}] -> No neighbors, filling seat')
                    updated_row.append('#')
                else:
                    # no change
                    updated_row.append(seat)
            elif seat == '#':
                # If seat is occupied: check if neighbors >= 5, empty if so
                n = count_visible_seats(seats, x, y)
                if n >= 5:
                    # print(f'{x}:{y} [{seat}] -> {n} neighbors, emptying seat')
                    updated_row.append('L')
                else:
                    # no change
                    updated_row.append(seat)
            else:
                # No change
                # print(f'{x}:{y} [{seat}] -> No change')
                updated_row.append(seat)
        updated_seats.append(updated_row)
    return updated_seats


def task2(lines):
    state = lines.copy()
    next_state = update_seats_task2(state)
    i = 0
    while next_state != state:
        if i == 60000:
            print('Iter limit reached')
            break
        state = next_state
        next_state = update_seats_task2(state)
        print('\r' + str(i), end='')
        i += 1
    print('\nFound stable state.')
    n = 0
    for row in state:
        for seat in row:
            if seat == '#':
                n += 1
    for row in state:
        print(''.join(row))
    print(f'Number of filled seats: {n}')


if __name__ == '__main__':
    with open('day11_input.txt', 'r') as f:
        lines = f.readlines()
    task2(lines)
